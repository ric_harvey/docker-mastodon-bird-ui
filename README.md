# docker-mastodon-bird-ui

This repo is used to build the latest docker image for mastodon and include the awesome theme by [@rolle](https://awscommunity.social/@rolle@mementomori.social) (be sure to give them a follow) called Bird-UI which makes mastodon feel like the long gone, good old days of twitter. You can find all the details of this theme here: [GitHub](https://github.com/ronilaukkarinen/mastodon-bird-ui)

It uses the default docker image directly from [mastodon](https://github.com/mastodon/mastodon) only modified by the instructions from @rolle. 

![The Bird UI Theme](./img/full.png)

## Getting started

### Building the image yourself

First check out this repository and pull the submodule in git:

```bash
git clone  https://gitlab.com/ric_harvey/docker-mastodon-bird-ui.git
git submodule update --init --recursive
```

You should then edit the build.sh file and replace ```--tag richarvey/mastodon-bird-ui:$mastodon_version --tag richarvey/mastodon-bird-ui:latest``` with the details of your own docker repository. Then you just need to run:

```bash
./build.sh
```

This script will download the theme and patch the locales file to display the options for choice, then build and push the docker images to your own repo.

### Pulling from docker

You can pull directly from docker hub with the following command:

```bash
docker pull richarvey/mastodon-bird-ui:latest
```

Starting with v4.2.9 of mastodon you can also pull by a specific tag version:

```bash
docker pull richarvey/mastodon-bird-ui:v4.2.9
```

### Running in docker compose

If you want to use the official docker compose method for running mastodon here is the modified code for **compose.yml**:

```yaml
# This file is designed for production server deployment, not local development work
# For a containerized local dev environment, see: https://github.com/mastodon/mastodon/blob/main/README.md#docker

services:
  db:
    restart: always
    image: postgres:14-alpine
    shm_size: 256mb
    networks:
      - internal_network
    healthcheck:
      test: ['CMD', 'pg_isready', '-U', 'postgres']
    volumes:
      - ./postgres14:/var/lib/postgresql/data
    environment:
      - 'POSTGRES_HOST_AUTH_METHOD=trust'

  redis:
    restart: always
    image: redis:7-alpine
    networks:
      - internal_network
    healthcheck:
      test: ['CMD', 'redis-cli', 'ping']
    volumes:
      - ./redis:/data

  # es:
  #   restart: always
  #   image: docker.elastic.co/elasticsearch/elasticsearch:7.17.4
  #   environment:
  #     - "ES_JAVA_OPTS=-Xms512m -Xmx512m -Des.enforce.bootstrap.checks=true"
  #     - "xpack.license.self_generated.type=basic"
  #     - "xpack.security.enabled=false"
  #     - "xpack.watcher.enabled=false"
  #     - "xpack.graph.enabled=false"
  #     - "xpack.ml.enabled=false"
  #     - "bootstrap.memory_lock=true"
  #     - "cluster.name=es-mastodon"
  #     - "discovery.type=single-node"
  #     - "thread_pool.write.queue_size=1000"
  #   networks:
  #      - external_network
  #      - internal_network
  #   healthcheck:
  #      test: ["CMD-SHELL", "curl --silent --fail localhost:9200/_cluster/health || exit 1"]
  #   volumes:
  #      - ./elasticsearch:/usr/share/elasticsearch/data
  #   ulimits:
  #     memlock:
  #       soft: -1
  #       hard: -1
  #     nofile:
  #       soft: 65536
  #       hard: 65536
  #   ports:
  #     - '127.0.0.1:9200:9200'

  web:
    image: richarvey:mastodon-bird-ui:latest
    restart: always
    env_file: .env.production
    command: bundle exec puma -C config/puma.rb
    networks:
      - external_network
      - internal_network
    healthcheck:
      # prettier-ignore
      test: ['CMD-SHELL',"curl -s --noproxy localhost localhost:3000/health | grep -q 'OK' || exit 1"]
    ports:
      - '127.0.0.1:3000:3000'
    depends_on:
      - db
      - redis
      # - es
    volumes:
      - ./public/system:/mastodon/public/system

  streaming:
    # You can uncomment the following lines if you want to not use the prebuilt image, for example if you have local code changes
    # build:
    #   dockerfile: ./streaming/Dockerfile
    #   context: .
    image: ghcr.io/mastodon/mastodon-streaming:v4.3.0
    restart: always
    env_file: .env.production
    command: node ./streaming/index.js
    networks:
      - external_network
      - internal_network
    healthcheck:
      # prettier-ignore
      test: ['CMD-SHELL', "curl -s --noproxy localhost localhost:4000/api/v1/streaming/health | grep -q 'OK' || exit 1"]
    ports:
      - '127.0.0.1:4000:4000'
    depends_on:
      - db
      - redis

  sidekiq:
    build: .
    image: ghcr.io/mastodon/mastodon:v4.3.0
    restart: always
    env_file: .env.production
    command: bundle exec sidekiq
    depends_on:
      - db
      - redis
    networks:
      - external_network
      - internal_network
    volumes:
      - ./public/system:/mastodon/public/system
    healthcheck:
      test: ['CMD-SHELL', "ps aux | grep '[s]idekiq\ 6' || false"]

  ## Uncomment to enable federation with tor instances along with adding the following ENV variables
  ## http_hidden_proxy=http://privoxy:8118
  ## ALLOW_ACCESS_TO_HIDDEN_SERVICE=true
  # tor:
  #   image: sirboops/tor
  #   networks:
  #      - external_network
  #      - internal_network
  #
  # privoxy:
  #   image: sirboops/privoxy
  #   volumes:
  #     - ./priv-config:/opt/config
  #   networks:
  #     - external_network
  #     - internal_network

networks:
  external_network:
  internal_network:
    internal: true
```

## Help needed
So far the English and Finish locales are patched so the theme appears in the preferences section under "Appearance". If you can provide a new translation in the *patches* directory for another language that would be most appreciated please submit a merge/pull request.

![Screenshot of the preferences/appearances screen](./img/preferences.png)

> There's no need to generate the diff file as ```build.sh``` generates these for you.
